rts
===
rts - as in `*r*eal-*t*ime *s*settings` is a script used by a |systemd| |systemd.service|, allowing to set pre-defined real-time related scheduling settings.
Its service files are dependant upon the real-time kernel, which means, they will only start if */sys/kernel/realtime* is present and not empty.
This software and all of its components are licensed under the GPLv3.

Requirements
------------
* |linux-rt|
* |tuna|

Usage
-----
| When starting the systemd service *rts.service*, the general settings file */etc/rts/rts* will be sourced.
| Using *rts@.service* a configuration file (by the name of the word following the *@*) below `/etc/rts/` will be used.
| Some examples can be found in `/etc/rts/examples`. For further settings and a general overview, please have a look at the |tuna_documentation|

.. |systemd| raw:: html

  <a href="https://en.wikipedia.org/wiki/Systemd" target="_blank">systemd</a>

.. |systemd.service| raw:: html

  <a href="http://www.freedesktop.org/software/systemd/man/systemd.service.html" target="_blank">service</a>

.. |real-time_kernel| raw:: html

  <a href="https://www.kernel.org/pub/linux/kernel/projects/rt/" target="_blank">real-time kernel</a>

.. |linux-rt| raw:: html

  <a href="https://aur.archlinux.org/packages/linux-rt/" target="_blank">linux-rt</a>

.. |kernel_parameter| raw:: html

  <a href="https://www.kernel.org/doc/Documentation/kernel-parameters.txt" target="_blank">kernel parameter</a>

.. |grub| raw:: html

  <a href="https://wiki.archlinux.org/index.php/Kernel_parameters#GRUB" target="_blank">GRUB</a>

.. |syslinux| raw:: html

  <a href="https://wiki.archlinux.org/index.php/Kernel_parameters#Syslinux" target="_blank">syslinux</a>

.. |systemd-boot| raw:: html

  <a href="https://wiki.archlinux.org/index.php/Kernel_parameters#systemd-boot" target="_blank">systemd-boot</a>

.. |tuna| raw:: html

  <a href="https://rt.wiki.kernel.org/index.php/Tuna" target="_blank">tuna</a>

.. |tuna_documentation| raw:: html

  <a href="https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_MRG/1.3/html-single/Tuna_User_Guide/index.html" target="_blank">tuna documentation</a>

